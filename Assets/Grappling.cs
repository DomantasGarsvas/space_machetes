﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grappling : MonoBehaviour
{
    Vector3 StartCoords;
    Vector3 StartRot;
    float ReloadTime = 1f;
    float NextShoot=0f;
    float ForcedReloadTime = 5f;
    float NextForcedReload=0f;
    bool hit=false;
    // Start is called before the first frame update
    void Start()
    {
        Rigidbody rb = gameObject.GetComponent<Rigidbody>();
        if (rb == null)
        {
            gameObject.AddComponent(typeof(Rigidbody));
            gameObject.GetComponent<Rigidbody>().useGravity = false;            
        }
        NextShoot = Time.time + ReloadTime;
        NextForcedReload = Time.time + NextForcedReload;
        StartCoords = gameObject.transform.position;
        StartRot = new Vector3(gameObject.transform.rotation.x, gameObject.transform.rotation.y, gameObject.transform.rotation.z);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0)&&Time.time>NextShoot||Input.GetMouseButtonDown(0)&&!hit&&Time.time>NextForcedReload)
        {
            hit = false;
            if(StartCoords!=gameObject.transform.position)
            {
                Reload();
            }
            //float tinydelay = Time.time + 0.2f;
            //while (Time.time < tinydelay) { if (Time.time > tinydelay) break; }
            Shoot();
            NextShoot = Time.time + ReloadTime;
            NextForcedReload = Time.time + ForcedReloadTime;
        }
        if(Time.time>NextForcedReload&&!hit)
        {
            Reload();
        }
    }
    void Shoot()
    {
        gameObject.GetComponent<Rigidbody>().AddForce(Vector3.forward * 20, ForceMode.Impulse);
        StartCoords = gameObject.transform.position;
        StartRot = new Vector3(gameObject.transform.rotation.x, gameObject.transform.rotation.y, gameObject.transform.rotation.z);
    }
    void Reload()
    {
        gameObject.transform.position = StartCoords;
    }
   
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("We Got Them");
        gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        hit = true;
    }
}
