﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public ModuleManager moduleManager;
    public PlayerController player;
    public Animation capsuleAnimation;
    public Volume volume;

    public GameObject fakeWindow;

    public string[] explanationLines;

    public string[] victoryLines;

    public string[] vineLooseLines;

    public string[] airLooseLines;

    public GameObject skipButtonRoot;
    public GameObject DialogueWindow;
    public GameObject crosshair;
    public TextMeshProUGUI textMesh;

    public GameObject playerPrefab;

    public float amountOfTimeBetweenTexts = 1f;
    public float startInfectingAfter = 35f;

    private UnityEngine.Rendering.Universal.Vignette vignette;
    private UnityEngine.Rendering.Universal.DepthOfField depthOfField;

    public int amountOfModules = 0;
    public int amountOfDamaged = 0;
    public int amountOfInfected = 0;

    public AudioClip typingSFX;
    public AudioSource audioSource;

    private VolumeComponent comp;

    private void Start()
    {
        GameStartPhase();
        GetPostProcessParameters();
    }

    private void GetPostProcessParameters()
    {
        vignette = new UnityEngine.Rendering.Universal.Vignette();
        depthOfField = new UnityEngine.Rendering.Universal.DepthOfField();
        if (volume.profile.TryGet(out depthOfField))
        {
            Debug.Log("dof got");            
        }

        if (volume.profile.TryGet(out vignette))
        {
            Debug.Log("vig got");
        }
    }

    void GameStartPhase()
    {
        StartCoroutine(ShowTexts(explanationLines));
        StartCoroutine(CapsuleIntro());
        moduleManager.GenerateModules();
    }

    public void UpdateObjectivesUI()
    {
        if (player != null)
        {
            string text = $"infected  modules:  {amountOfInfected}/{amountOfModules}";
            text += Environment.NewLine;
            text += $"damaged  modules:  {amountOfDamaged}/{amountOfModules}";

            player.UpdateObjectives(text);
        }
    }

    public void SkipIntro()
    {
        StopAllCoroutines();
        audioSource.loop = false;
        audioSource.Stop();

        if (!moduleManager.hasInfected)
        {
            moduleManager.InfectModules(0f);
        }
        Cursor.visible = false;
        DialogueWindow.SetActive(false);
        skipButtonRoot.SetActive(false);
        fakeWindow.SetActive(false);
        GameObject playerObject = Instantiate(playerPrefab, capsuleAnimation.transform.TransformPoint(0, 0, 1.2f), capsuleAnimation.transform.rotation);
        player = playerObject.GetComponent<PlayerController>();
        player.InitiatePlayer();
        player.UnfrezePlayer();
        crosshair.SetActive(true);
    }

    private IEnumerator ShowTexts(string[] texts)
    {
        DialogueWindow.SetActive(true);
        audioSource.clip = typingSFX;
        audioSource.loop = true;
        foreach (string text in texts)
        {
            audioSource.Play();
            yield return PrintOneLetterAtTime(text);
            audioSource.Stop();
            yield return new WaitForSeconds(amountOfTimeBetweenTexts);
        }
        audioSource.loop = false;
        audioSource.Stop();
        DialogueWindow.SetActive(false);
    }

    private IEnumerator PrintOneLetterAtTime(string text)
    {
        string showedText = "";
        int length = text.Length;
        foreach (char character in text)
        {
            showedText += character;
            textMesh.text = showedText;
            yield return new WaitForSeconds(0.08f);
        }
    }

    private IEnumerator CapsuleIntro()
    {
        capsuleAnimation.Play();
        yield return new WaitForSeconds(capsuleAnimation.clip.length / 4 * 3);
        moduleManager.InfectModules(0f);
        yield return new WaitForSeconds(capsuleAnimation.clip.length / 4);
        Cursor.visible = false;
        skipButtonRoot.SetActive(false);
        fakeWindow.SetActive(false);
        GameObject playerObject = Instantiate(playerPrefab, capsuleAnimation.transform.TransformPoint(0, 0, 1.2f), capsuleAnimation.transform.rotation);
        player = playerObject.GetComponent<PlayerController>();
        player.GetComponentInChildren<PostProcessLayer>().volumeTrigger = transform;
        player.InitiatePlayer();
        player.UnfrezePlayer();
        crosshair.SetActive(true);

    }
    public void GameWon()
    {
        crosshair.SetActive(false);
        Debug.Log("YOU FIXED THE STATION!");
        player.FreezeMovement();
        StartCoroutine(WinPhase());
    }

    private IEnumerator WinPhase()
    {
        StartCoroutine(FadeBlurOut());
        yield return ShowTexts(victoryLines);
        MoveToMenu();
    }


    public void AirLoose()
    {
        crosshair.SetActive(false);
        Debug.Log("AIR RAN OUT YOU LOOSE!");
        player.FreezeMovement();
        StartCoroutine(LoosePhaseAir());
    }

    private IEnumerator LoosePhaseAir()
    {
        StartCoroutine(FadeBlurOut());
        yield return ShowTexts(airLooseLines);
        MoveToMenu();
    }

    internal void VineLoose()
    {
        crosshair.SetActive(false);
        Debug.Log("STATION INFECTED YOU LOOSE!");
        player.FreezeMovement();
        StartCoroutine(LoosePhaseVines());
    }

    private IEnumerator LoosePhaseVines()
    {
        StartCoroutine(FadeBlurOut());
        yield return ShowTexts(vineLooseLines);
        MoveToMenu();
    }

    private IEnumerator FadeBlurOut()
    {
        float blurAmount = 1;
        float fadeAmount = 0.2f;

        bool doFade = true;
        while (doFade)
        {
            vignette.intensity.value = fadeAmount;
            depthOfField.focusDistance.value = blurAmount;

            fadeAmount += 0.006f;
            blurAmount -= 0.01f;

            yield return new WaitForSeconds(0.05f);

            if (fadeAmount > 3)
            {
                doFade = false;
            }
        }
    }

    private void MoveToMenu()
    {
        SceneManager.LoadScene("MenuScene");
        Cursor.visible = true;
    }
}