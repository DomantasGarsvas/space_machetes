﻿using System.Collections;
using UnityEngine;

public class LightBlink : MonoBehaviour
{
    public AnimationCurve animCurve;

    public bool blink;

    public Light blinkyLight;

    void Start()
    {
        blink = true;
        StartCoroutine(Blink());
    }

    private IEnumerator Blink()
    {
        Debug.Log("Starting blink");
        float value = 0;
        while (blink)
        {
            blinkyLight.intensity = 0.4f + (animCurve.Evaluate(value) / 2);
            yield return new WaitForSeconds(0.05f);
            value += 0.05f;
        }
    }
}