﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuStation : MonoBehaviour
{
    public float RotSpeed = 1f;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        float rand = Random.Range(0f, 1f);
        if(rand < 0.3)
        {
            transform.Rotate(Vector3.up * RotSpeed * Time.deltaTime);
            //Debug.Log("Up");
        }
        else if (rand < 0.6)
        {
            //Debug.Log("Right");
            transform.Rotate(Vector3.right * RotSpeed * Time.deltaTime);
        }
        else
        {
            //Debug.Log("Forward");
            transform.Rotate(Vector3.forward * RotSpeed * Time.deltaTime);
        }

    }
}
