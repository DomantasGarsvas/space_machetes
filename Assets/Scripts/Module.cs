﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Module : MonoBehaviour
{
    public GameObject root;
    public Collider coll;
    public GameObject[] docks;
    public Renderer mainRenderer;

    public GameObject vinePrefab;
    private GameObject vineObject;
    private bool isInfecting;

    public List<Module> neighbours;
    public bool infected;
    public float infectHealth = 100f;
    private float currentHealth;
    public float infectTime;
    public float infectChance;
    public float generateChance;
    public int id;

    public int maxOccupiedDocks;
    public int prefferedDock = -1;
    public float preferenceValue = 1f;
    public float degradeRate;
    public float degradeAmount = 0;
    public float degradeCap = 100;

    private Coroutine infection;
    private Coroutine degradation;

    private ModuleManager manager;

    public void Initiate(float infectTime, float infectChance, float degradeRate, float generateChance, ModuleManager moduleManager, Module previous)
    {
        manager = moduleManager;
        manager.RegisterModule(this);
        CheckIfIntercepts(manager);

        this.infectChance = infectChance;
        this.infectTime = infectTime;
        this.degradeRate = degradeRate;
        isInfecting = false;

        neighbours = new List<Module>();


        this.generateChance = manager.generationValues.Evaluate(generateChance);
        float newgenerateChance = generateChance + manager.chanceIncrement;

        StartCoroutine(GenerateExtraModules(manager.modulePrefabs, newgenerateChance, manager, previous));
    }

    private void CheckIfIntercepts(ModuleManager manager)
    {
        GameObject[] allModules = GameObject.FindGameObjectsWithTag("Module");

        foreach (GameObject module in allModules)
        {
            if (id != module.GetComponent<Module>().id)
            {
                if (coll.bounds.Intersects(module.GetComponent<Module>().coll.bounds))
                {
                    Debug.Log($"{id} is intersecting {module.GetComponent<Module>().id}");
                    manager.UnregisterModule(this);
                    Destroy(root);
                    break;
                }
            }
        }
    }

    public IEnumerator GenerateExtraModules(GameObject[] modules, float newChance, ModuleManager manager, Module previous)
    {
        yield return new WaitForSeconds(0.05f);

        if (previous != null)
        {
            neighbours.Add(previous);
        }

        int generated = 0;
        int index = 0;
        foreach (GameObject dock in docks)
        {
            if (index == prefferedDock)
            {
                if (Random.value < generateChance * preferenceValue)
                {
                    GameObject module = Instantiate(modules[Random.Range(0, modules.Length - 1)], dock.transform.TransformPoint(0, 0, 0), dock.transform.rotation);
                    module.GetComponentInChildren<Module>().Initiate(infectTime, infectChance, degradeRate, newChance, manager, this);
                    neighbours.Add(module.GetComponentInChildren<Module>());
                    generated++;
                }
            }
            if (Random.value < generateChance)
            {
                GameObject module = Instantiate(modules[Random.Range(0, modules.Length - 1)], dock.transform.TransformPoint(0, 0, 0), dock.transform.rotation);
                module.GetComponentInChildren<Module>().Initiate(infectTime, infectChance, degradeRate, newChance, manager, this);
                neighbours.Add(module.GetComponentInChildren<Module>());
                generated++;
            }

            if (generated >= maxOccupiedDocks)
            {
                break;
            }

            index++;
        }
    }

    public void InfectModule(bool firstTime = true)
    {
        if (!infected)
        {
            currentHealth = infectHealth;
            Debug.Log($"Module {id} infected!");

            if (firstTime)
            {
                ChangeAppearance();
                infected = true;
            }

            if (infection != null)
            {
                StopCoroutine(infection);
            }

            if (degradation != null)
            {
                StopCoroutine(degradation);
            }

            infection = StartCoroutine(CheckForInfect());
            degradation = StartCoroutine(Degrade());

            manager.TryAddModuleToInfected(this);
            manager.TryAddModuleToDamaged(this);
        }
    }

    private IEnumerator Degrade()
    {
        Debug.Log($"Degrade started on module {id}");

        while (infected && degradeAmount < degradeCap)
        {
            yield return new WaitForSeconds(10f);
            degradeAmount += degradeRate;
            mainRenderer.material.SetFloat("_value", degradeAmount / degradeCap);
            Debug.Log($"updating mainRenderer.material of module {id} to value {degradeAmount / degradeCap}");
        }
    }

    public void Fix(float amount)
    {
        if (degradeAmount > 0)
        {
            Debug.Log($"Module {id} fixed for {amount}");
            degradeAmount -= amount;

        }
        else
        {
            Debug.Log($"Module {id} is already fixed!");
            manager.TryRemoveModuleFromDamaged(this);
        }
    }

    public void AttackInfection(float damage)
    {
        Debug.Log($"Module {id} attacked for {damage}");
        currentHealth -= damage;
        if (currentHealth <= 0)
        {
            Disinfect();
        }
    }

    private void Disinfect()
    {
        infected = false;
        Destroy(vineObject);
        NotifyNeighboursToStartInfectingAgain();
        Debug.Log($"Module {id} disinfected");
        manager.TryRemoveModuleFromInfected(this);
    }

    private void NotifyNeighboursToStartInfectingAgain()
    {
        foreach (Module module in neighbours)
        {
            if (module != null)
            {
                if (module.infected && !module.isInfecting)
                {
                    module.InfectModule(false);
                }
            }
        }
    }

    private void ChangeAppearance()
    {
        if (vinePrefab != null)
        {
            vineObject = Instantiate(vinePrefab, transform.TransformPoint(0, 0, 0), transform.rotation);
        }
    }

    private IEnumerator CheckForInfect()
    {
        isInfecting = true;
        bool infectSuccesfull = true;
        while (infectSuccesfull)
        {

            yield return new WaitForSeconds(infectTime);
            if (Random.value <= infectChance)
            {
                infectSuccesfull = InfectRandomNeighbour();
            }
        }
        isInfecting = false;
        Debug.Log("All modules infected!");
    }

    private bool InfectRandomNeighbour()
    {
        int index;

        if (neighbours.Count > 0)
        {

            Module[] modules = NonInfectedArray();
            if (modules.Length > 0)
            {
                Debug.Log("Infecting random neighbour");
                index = Random.Range(0, modules.Length - 1);
                modules[index].InfectModule();
                return true;
            }
            else
            {
                Debug.Log("No clean neighbours!");
                return false;
            }
        }
        else
        {
            Debug.Log("No neighbours to infect!");
            return false;
        }
    }

    private Module[] NonInfectedArray()
    {
        List<Module> notInfected = new List<Module>();
        foreach (Module module in neighbours)
        {
            if (module != null)
            {
                if (!module.infected)
                {
                    notInfected.Add(module);
                }
            }
        }
        return notInfected.ToArray();
    }
}