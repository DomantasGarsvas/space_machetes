﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModuleManager : MonoBehaviour
{
    public GameObject[] modulePrefabs;
    public GameObject startingModule;

    public List<Module> modules;
    private int id;

    public List<Module> infected;
    public List<Module> damaged;

    public AnimationCurve generationValues;
    public float chanceIncrement;

    public float infectTime;
    public float infectChance;
    public float degradeRate;

    public int infectAmount;
    public bool hasInfected;

    public bool gameEnded = false;

    private void Start()
    {
        modules = new List<Module>();
        infected = new List<Module>();
        damaged = new List<Module>();

        id = 0;
        hasInfected = false;
    }

    public void GenerateModules()
    {
        startingModule.GetComponentInChildren<Module>().Initiate(infectTime, infectChance, degradeRate, 0, this, null);
    }

    public void InfectModules(float infectTime)
    {
        hasInfected = true;
        StartCoroutine(InfectRandomModuleAfterTime(infectAmount, infectTime));
        StartCoroutine(CheckForAllModulesInfected(infectTime + 1f));
        FindObjectOfType<GameManager>().amountOfModules = modules.Count+1;
    }

    public void RegisterModule(Module module)
    {
        module.id = id;
        module.gameObject.name = "Module_" + id.ToString();
        modules.Add(module);

        Debug.Log($"registered module {id}");

        id++;
    }

    public IEnumerator InfectRandomModuleAfterTime(int amountOfModules, float seconds)
    {
        yield return new WaitForSeconds(seconds);
        for (int i = 0; i < amountOfModules; i++)
        {
            int random = Random.Range(1, modules.Count);
            if (!modules[random].infected)
            {
                modules[random].InfectModule();
                yield return new WaitForSeconds(0.5f);
            }
            else
            {
                i--;
            }
        }
    }

    private IEnumerator CheckForAllModulesInfected(float time)
    {
        yield return new WaitForSeconds(time);
        while (!gameEnded)
        {
            yield return new WaitForSeconds(1f);
            if (CheckAllModulesForInfection(true))
            {
                gameEnded = true;
                FindObjectOfType<GameManager>().VineLoose();
            }
            else if (CheckAllModulesForInfection(false))
            {
                if (CheckAllModulesForRepair())
                {
                    gameEnded = true;
                    FindObjectOfType<GameManager>().GameWon();
                }
            }
        }
    }

    private bool CheckAllModulesForRepair()
    {
        foreach (Module module in modules)
        {
            if (module.degradeAmount > 0)
                return false;
        }
        return true;
    }

    private bool CheckAllModulesForInfection(bool infected)
    {
        foreach (Module module in modules)
        {
            if (module.infected == !infected)
                return false;
        }
        return true;
    }

    public void UnregisterModule(Module module)
    {
        Debug.Log($"Deregistered module {module.id}");
        modules.Remove(module);
    }

    internal void TryAddModuleToInfected(Module module)
    {
        if (infected.Contains(module))
        {
            Debug.Log($"module {id} is already in infected list");
        }
        else
        {
            infected.Add(module);
            FindObjectOfType<GameManager>().amountOfInfected = infected.Count;
            FindObjectOfType<GameManager>().UpdateObjectivesUI();
        }
    }

    internal void TryAddModuleToDamaged(Module module)
    {
        if (damaged.Contains(module))
        {
            Debug.Log($"module {id} is already in damaged list");
        }
        else
        {
            damaged.Add(module);
            FindObjectOfType<GameManager>().amountOfDamaged = damaged.Count;
            FindObjectOfType<GameManager>().UpdateObjectivesUI();
        }
    }

    internal void TryRemoveModuleFromDamaged(Module module)
    {
        if (damaged.Contains(module))
        {
            damaged.Remove(module);
            FindObjectOfType<GameManager>().amountOfDamaged = damaged.Count;
            FindObjectOfType<GameManager>().UpdateObjectivesUI();
        }
        else
        {
            Debug.Log($"module {id} is not in the damaged list");
        }
    }

    internal void TryRemoveModuleFromInfected(Module module)
    {
        if (infected.Contains(module))
        {
            infected.Remove(module);
            FindObjectOfType<GameManager>().amountOfInfected = infected.Count;
            FindObjectOfType<GameManager>().UpdateObjectivesUI();
        }
        else
        {
            Debug.Log($"module {id} is not in the infected list");
        }
    }
}