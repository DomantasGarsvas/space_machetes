﻿using System.Collections;
using UnityEngine;

public class Music : MonoBehaviour
{
    public AudioClip coreFile;
    public AudioClip[] clips;

    private int index;
    private int previousIndex;

    public bool play = true;

    public AudioSource audioSource;
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
        audioSource.clip = coreFile;
        audioSource.loop = true;
        audioSource.Play();
        previousIndex = -1;
        Debug.Log("started playing core music");
        StartCoroutine(PlayVariations());
    }

    private IEnumerator PlayVariations()
    {   
        while (play)
        {
            index = GetRandomNext();
            yield return PlayClip(clips[index]);
        }
    }

    private int GetRandomNext()
    {
        while (true)
        {
            if(index != previousIndex)
            {
                return index;
            }

            index = Random.Range(0, clips.Length - 1);
        }
    }

    private IEnumerator PlayClip(AudioClip clip)
    {
        Debug.Log($"started clip {clip.name}");
        audioSource.PlayOneShot(clip);
        yield return new WaitForSeconds(clip.length);
    }
}