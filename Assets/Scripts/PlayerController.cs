﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float movementSpeed;
    public float rotationSpeed;

    public float drag = 0.2f;

    public float lookXSensitivity;
    public float lookYSensitivity;

    public float fuelAmount;
    public float airAmount;

    private float maxAirAmount;
    private float maxFuelAmount;

    public bool usingAir = false;
    public bool usingFuel = false;

    public float fuelUsageRate;
    public float airUsageRate;

    public GameObject root;
    public GameObject playerCamera;

    private Rigidbody rb;

    public KeyCode forwards;
    public KeyCode backwards;
    public KeyCode left;
    public KeyCode right;
    public KeyCode up;
    public KeyCode down;

    public KeyCode rotateLeft;
    public KeyCode rotateRight;

    public TextMeshPro airText;
    public TextMeshPro fuelText;
    public TextMeshPro objectives;

    public float timeForObjectives;
    private bool canUpdateObjectives;

    private bool paused;

    void Update()
    {
        if (!paused)
        {
            Move();
            Rotate();
        }
    }

    public void UpdateObjectives(string text)
    {
        if (canUpdateObjectives)
        {
            objectives.text = text;
        }
    }

    private IEnumerator ShowObjectives()
    {
        string text = "OBJECTIVES:" + Environment.NewLine + Environment.NewLine;
        text += "1.  remove  infestation" + Environment.NewLine;
        text += "2.  fix  damaged  modules";

        objectives.text = text;
        yield return new WaitForSeconds(timeForObjectives);
        canUpdateObjectives = true;
        FindObjectOfType<GameManager>().UpdateObjectivesUI();
    }

    public void InitiatePlayer()
    {
        canUpdateObjectives = false;
        StartCoroutine(ShowObjectives());
        paused = true;
        rb = GetComponent<Rigidbody>();
        maxAirAmount = airAmount;
        maxFuelAmount = fuelAmount;
    }


    public void UnfrezePlayer()
    {
        paused = false;
        usingAir = true;
        StartCoroutine(AirUsage());
    }

    private void Move()
    {
        Vector3 move = new Vector3();

        if (Input.GetKey(forwards))
        {
            move += Vector3.forward;
        }
        if (Input.GetKey(backwards))
        {
            move += Vector3.back;
        }
        if (Input.GetKey(left))
        {
            move += Vector3.left;
        }
        if (Input.GetKey(right))
        {
            move += Vector3.right;
        }
        if (Input.GetKey(up))
        {
            move += Vector3.up;
        }
        if (Input.GetKey(down))
        {
            move += Vector3.down;
        }
        if (fuelAmount <= 0)
        {
            fuelAmount = 0;
            rb.drag = 0;
            fuelText.text = "0%";
        }
        else
        {
            fuelAmount -= move.sqrMagnitude * Time.deltaTime * fuelUsageRate;
            rb.AddRelativeForce(move * movementSpeed, ForceMode.Acceleration);
            fuelText.text = ((int)fuelAmount).ToString() + "%";
        }
    }
    private void Rotate()
    {
        Vector3 rotate = new Vector3(-Input.GetAxis("Mouse Y") * lookYSensitivity, Input.GetAxis("Mouse X") * lookXSensitivity, 0);

        if (Input.GetKey(rotateLeft))
        {
            rotate += Vector3.forward;
        }
        if (Input.GetKey(rotateRight))
        {
            rotate += Vector3.back;
        }

        rb.AddRelativeTorque(rotate * rotationSpeed, ForceMode.Acceleration);
    }

    public void FreezeMovement()
    {
        StopAllCoroutines();
        paused = true;
        GetComponent<ToolScript>().canUse = false;
    }

    #region resources

    private IEnumerator AirUsage()
    {
        while (usingAir)
        {
            airAmount -= airUsageRate / 10;
            airText.text = ((int)airAmount).ToString() + "%";
            yield return new WaitForSeconds(0.1f);
            if (airAmount <= 0)
            {
                airAmount = 0;
                airText.text = ((int)airAmount).ToString() + "%";
                usingAir = false;
                FindObjectOfType<GameManager>().AirLoose();
            }
        }
    }

    private IEnumerator AirRefill()
    {
        while (!usingAir)
        {
            if (airAmount >= maxAirAmount)
            {
                airAmount = maxAirAmount;
            }
            else
            {
                airAmount += airUsageRate;
            }
            airText.text = ((int)airAmount).ToString() + "%";
            yield return new WaitForSeconds(0.1f);
        }
    }

    private IEnumerator FuelRefill()
    {
        rb.drag = drag;
        while (!usingFuel)
        {
            if (fuelAmount + fuelUsageRate >= maxFuelAmount)
            {
                fuelAmount = maxFuelAmount;
            }
            else
            {
                fuelAmount += fuelUsageRate;
            }
            fuelText.text = ((int)fuelAmount).ToString() + "%";
            yield return new WaitForSeconds(0.1f);
        }
    }

    public void StartAirRefill()
    {
        Debug.Log("Starting air fill");
        usingAir = false;
        StartCoroutine(AirRefill());
    }

    public void StopAirRefill()
    {
        Debug.Log("Stopping air fill");
        usingAir = true;
        StartCoroutine(AirUsage());
    }

    public void StartFuelRefill()
    {
        Debug.Log("Starting fuel fill");
        usingFuel = false;
        StartCoroutine(FuelRefill());
    }

    public void StopFuelRefill()
    {
        Debug.Log("Stopping fuel fill");
        usingFuel = true;
    }
    #endregion
}