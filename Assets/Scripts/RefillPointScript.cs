﻿using UnityEngine;

public class RefillPointScript : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            PlayerController player = other.GetComponent<PlayerController>();
            player.StartAirRefill();
            player.StartFuelRefill();

        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            PlayerController player = other.GetComponent<PlayerController>();
            player.StopAirRefill();
            player.StopFuelRefill();

        }
    }
}