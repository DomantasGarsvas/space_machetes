﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartButton : MonoBehaviour
{

    public GameObject definedButton;
    public UnityEvent OnClick = new UnityEvent();
    public float floatStrength = 10;
    float originalY;
    public Animator animator;

    // Use this for initialization
    void Start()
    {
    }

    public void Invoke()
    {
        //Debug.Log("Button Clicked");
    }
    // Update is called once per frame
    void Update()
    {
        
    }

}
