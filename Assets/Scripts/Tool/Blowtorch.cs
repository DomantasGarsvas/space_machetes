﻿using UnityEngine;

public class Blowtorch : Tool
{
    public override void ToolAction()
    {
        if (Physics.Raycast(playerPos.position, playerPos.TransformDirection(Vector3.forward), out RaycastHit hit, reach))
        {
            Debug.DrawRay(playerPos.position, playerPos.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            if (hit.collider.gameObject.CompareTag("Module"))
            {
                Module module = hit.collider.gameObject.GetComponent<Module>();
                if (module.degradeAmount > 0)
                {
                    module.Fix(20f);
                }
                else
                {
                    Debug.Log("healthy Module Hit");
                }
            }
            else
            {
                Module module = hit.collider.GetComponentInParent<Module>();
                if (module != null)
                {
                    if (module.degradeAmount > 0)
                    {
                        module.Fix(20f);
                    }
                    else
                    {
                        Debug.Log("healthy Module Hit");
                    }
                }
                else
                {
                    Debug.Log("Not module Hit");
                }
            }
        }
        else
        {
            Debug.DrawRay(playerPos.position, playerPos.TransformDirection(Vector3.forward) * 1000, Color.white);
            Debug.Log("Did not Hit");
        }

        base.ToolAction();
    }
}
