﻿using UnityEngine;

public class Machete : Tool
{
    public override void ToolAction()
    {
        if (Physics.Raycast(playerPos.position, playerPos.TransformDirection(Vector3.forward), out RaycastHit hit, reach))
        {
            Debug.DrawRay(playerPos.position, playerPos.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            if (hit.collider.gameObject.CompareTag("Module"))
            {
                playerPos.gameObject.GetComponent<AudioSource>().PlayOneShot(soundEffectsOnHit[Random.Range(0, soundEffectsOnHit.Length - 1)]);
                Module module = hit.collider.gameObject.GetComponent<Module>();
                if (module.infected)
                {
                    module.AttackInfection(25f);
                }
                else
                {
                    Debug.Log("non infected Module Hit");
                }
            }
            else
            {
                Module module = hit.collider.GetComponentInParent<Module>();
                if (module != null)
                {
                    playerPos.gameObject.GetComponent<AudioSource>().PlayOneShot(soundEffectsOnHit[Random.Range(0, soundEffectsOnHit.Length - 1)]);
                    if (module.infected)
                    {
                        module.AttackInfection(25f);
                    }
                    else
                    {
                        Debug.Log("non infected Module Hit");
                    }
                }
                else
                {
                    Debug.Log("Not module Hit");
                }
            }
        }
        else
        {
            Debug.DrawRay(playerPos.position, playerPos.TransformDirection(Vector3.forward) * 1000, Color.white);
            Debug.Log("Did not Hit");
        }

        base.ToolAction();
    }
}