﻿using System.Collections;
using UnityEngine;

public class Tool : MonoBehaviour, IToolData
{
    public float reach;

    public float reloadSpeed;

    public KeyCode button;

    public Animation anim;

    public GameObject particleGameObject;

    public GameObject model;

    public bool isReloaded;

    public Transform playerPos;

    public AudioClip[] soundEffectsOnUse;

    public AudioClip[] soundEffectsOnHit;

    private void Start()
    {
        StartCoroutine(Reload());
    }

    virtual public void ToolAction()
    {
        Debug.Log("Tool used!");

        if (soundEffectsOnUse != null)
        {
            playerPos.gameObject.GetComponent<AudioSource>().PlayOneShot(soundEffectsOnUse[Random.Range(0, soundEffectsOnUse.Length-1)]);
            Debug.Log("Sound is played!");
        }
        else
        {
            Debug.Log("SOUND IS NULL!");
        }

        if (anim != null)
        {
            anim.Play();
            Debug.Log("Animation is played!");
        }
        else
        {
            Debug.Log("ANIMATION NULL!");
        }

        if (particleGameObject != null)
        {
            particleGameObject.SetActive(true);
            particleGameObject.GetComponent<ParticleSystem>().Play();
            Debug.Log("Particles is played!");
        }
        else
        {
            Debug.Log("PARTICLES NULL!");
        }
        StartCoroutine(Reload());
    }

    public IEnumerator Reload()
    {
        isReloaded = false;
        yield return new WaitForSeconds(reloadSpeed);
        isReloaded = true;
    }
}

public interface IToolData
{
    void ToolAction();
}