﻿using System.Collections.Generic;
using UnityEngine;

public class ToolScript : MonoBehaviour
{
    public Tool[] tools;
    public List<KeyCode> keys;
    public Transform root;

    private GameObject currentToolObject;
    private int currentToolIndex;

    public bool canUse;

    void Start()
    {
        keys = new List<KeyCode>();
        foreach (Tool data in tools)
        {
            data.playerPos = transform;
            keys.Add(data.button);
        }

        ChooseTool(0);
        canUse = true;
    }

    void Update()
    {
        if (canUse)
        {
            TryChooseTool();
            DoToolAction();
        }
    }

    private void DoToolAction()
    {
        if (Input.GetMouseButton(0))
        {
            if (currentToolObject.GetComponent<Tool>().isReloaded)
            {
                currentToolObject.GetComponent<Tool>().ToolAction();
            }
        }
    }

    private void TryChooseTool()
    {
        int index = 0;
        foreach (KeyCode key in keys)
        {
            if (Input.GetKey(key))
            {
                if (currentToolIndex != index)
                {
                    ChooseTool(index);
                }
                break;
            }
            index++;
        }
    }

    public void ChooseTool(int index)
    {
        if (currentToolObject != null)
        {
            Destroy(currentToolObject);
        }
        currentToolObject = Instantiate(tools[index].model, root);
        currentToolIndex = index;
    }
}