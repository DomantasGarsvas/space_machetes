﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheatleyOrbit : MonoBehaviour
{
    public GameObject Wheatley;
    public float RotSpeed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.forward * RotSpeed * Time.deltaTime);
        Wheatley.transform.Rotate(Vector3.right * RotSpeed * Time.deltaTime * 2);
    }
}
